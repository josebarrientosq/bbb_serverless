import json
import urllib.parse
import hashlib
import boto3
import os
import datetime

dynamodbClient = boto3.client("dynamodb",region_name='us-east-2')
ssmClient = boto3.client("ssm",region_name='us-east-2')
s3Client = boto3.client("s3",region_name='us-east-2')
TABLA_SERVIDOR = os.environ['TABLA_SERVIDOR']

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

def crearReunion(event, context):
    body = urllib.parse.parse_qs(event['body'])
    meeting_id = body['meeting_id'][0]
    nombre_sala = body['nombre_sala'][0]
    clave_moderador = body['clave_moderador'][0]
    clave_usuario = body['clave_usuario'][0]

    servidor = dynamodbClient.scan(
        TableName=TABLA_SERVIDOR,
        FilterExpression="estado=:a",
        ExpressionAttributeValues={
            ':a': {
                'S': 'disponible',
            }

        },
    )
    print(servidor)

    if servidor['Count']== 0:
        response = {
            "instance_id" : False,
            "mensaje": 'No hay servidores disponibles',
        }
        response = {
            "statusCode": 200,
            "body": json.dumps(response)
        }
        print(response)

        return response

    instance_id = servidor['Items'][0]['instance_id']['S']
    dominio = servidor['Items'][0]['subdominio']['S']
    api_secret = servidor['Items'][0]['apisecret']['S']

    response = {
        "url_crear_reunion" : generar_url_crear_reunion(dominio,api_secret,nombre_sala,meeting_id,clave_moderador,clave_usuario),
        "instance_id" : instance_id
    }
    """
    update_response = dynamodbClient.update_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id': {
                'S': instance_id,
            }
        },
        UpdateExpression="set estado=:a",
        ExpressionAttributeValues={
            ':a': {
                'S': 'ocupado',
            }

        },
        ReturnValues="UPDATED_NEW"
    )
    
    print (update_response)
    """
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response

def unirseReunionModerador(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    meeting_id = body['meeting_id'][0]
    nombre_moderador = body['nombre_moderador'][0]
    clave_moderador = body['clave_moderador'][0]

    servidor = dynamodbClient.get_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id' : {
                'S': instance_id,
            }

        }
    )
    print(servidor)

    dominio = servidor['Item']['subdominio']['S']
    api_secret = servidor['Item']['apisecret']['S']

    response = {
        "url_unirse_moderador" : generar_url_unirse_reunion_moderador(dominio,api_secret,nombre_moderador,meeting_id,clave_moderador),
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response

def unirseReunionUsuario(event, context):
    body = urllib.parse.parse_qs(event['body'])
    print(body)
    instance_id = body['instance_id'][0]
    meeting_id = body['meeting_id'][0]
    nombre_usuario = body['nombre_usuario'][0]
    clave_usuario = body['clave_usuario'][0]

    servidor = dynamodbClient.get_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id': {
                'S': instance_id,
            }
        }
    )
    print(servidor)
    dominio = servidor['Item']['subdominio']['S']
    api_secret = servidor['Item']['apisecret']['S']

    response = {
        "url_unirse_usuario" : generar_url_unirse_reunion_usuario(dominio,api_secret,meeting_id,nombre_usuario,clave_usuario),
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)
    return response

def verInformacionReunion(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    meeting_id = body['meeting_id'][0]
    clave_moderador = body['clave_moderador'][0]

    servidor = dynamodbClient.get_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id' : {
                'S': instance_id,
            }

        }
    )
    print(servidor)

    dominio = servidor['Item']['subdominio']['S']
    api_secret = servidor['Item']['apisecret']['S']

    response = {
        "url_ver_informacion_reunion" : generar_url_ver_informacion_reunion(dominio,api_secret,meeting_id,clave_moderador),
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response

def terminarReunion(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    meeting_id = body['meeting_id'][0]
    clave_moderador = body['clave_moderador'][0]

    servidor = dynamodbClient.get_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id' : {
                'S': instance_id,
            }
        }
    )
    print(servidor)

    dominio = servidor['Item']['subdominio']['S']
    api_secret = servidor['Item']['apisecret']['S']

    response = {
        "url_terminar_reunion" : generar_url_terminar_reunion(dominio,api_secret,meeting_id,clave_moderador),
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response

def generar_url_crear_reunion(dominio,api_secret,nombresala,meeting_id,clave_moderador,clave_usuario):
    url= 'https://'+dominio+'/bigbluebutton/api/'
    api_call='create'
    clave= api_secret
    query = 'name='+urllib.parse.quote(nombresala)+'&'+'meetingID='+meeting_id+'&'+'moderatorPW='+clave_moderador+'&'+'attendeePW='+clave_usuario+'&'+'welcome='+urllib.parse.quote("Bienvenidos")

    query2= api_call+query+clave
    checksum= hashlib.sha1(query2.encode()).hexdigest()

    url_create= url+api_call+'?'+query+'&checksum='+checksum
    return url_create

def generar_url_unirse_reunion_moderador(dominio,api_secret,nombre_moderador,meeting_id,clave_moderador):
    url= 'https://'+dominio+'/bigbluebutton/api/'
    api_call='join'
    clave= api_secret
    query = 'password='+clave_moderador+'&'+'fullName='+urllib.parse.quote(nombre_moderador)+'&'+'meetingID='+meeting_id
    query2= api_call+query+clave
    checksum= hashlib.sha1(query2.encode()).hexdigest()

    url_join_moderador= url+api_call+'?'+query+'&checksum='+checksum
    return url_join_moderador

def generar_url_unirse_reunion_usuario(dominio,api_secret,meeting_id,nombre_usuario,clave_usuario):
    url= 'https://'+dominio+'/bigbluebutton/api/'
    api_call='join'
    clave= api_secret
    query = 'password='+clave_usuario+'&'+'fullName='+urllib.parse.quote(nombre_usuario)+'&'+'meetingID='+meeting_id
    query2= api_call+query+clave
    checksum= hashlib.sha1(query2.encode()).hexdigest()

    url_join_usuario= url+api_call+'?'+query+'&checksum='+checksum
    return url_join_usuario

def generar_url_ver_informacion_reunion(dominio,api_secret,meeting_id,clave_moderador):
    url= 'https://'+dominio+'/bigbluebutton/api/'
    api_call='getMeetingInfo'
    clave= api_secret
    query = 'password='+clave_moderador+'&'+'meetingID='+meeting_id
    query2= api_call+query+clave
    checksum= hashlib.sha1(query2.encode()).hexdigest()

    url_getMeetingInfo= url+api_call+'?'+query+'&checksum='+checksum
    return url_getMeetingInfo

def generar_url_terminar_reunion(dominio,api_secret,meeting_id,clave_moderador):
    url= 'https://'+dominio+'/bigbluebutton/api/'
    api_call='end'
    clave= api_secret
    query = 'password='+clave_moderador+'&'+'meetingID='+meeting_id
    query2= api_call+query+clave
    checksum= hashlib.sha1(query2.encode()).hexdigest()

    url_terminar_reunion= url+api_call+'?'+query+'&checksum='+checksum
    return url_terminar_reunion

def liveRecording(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    meet_url = body['url_meet'][0]
    name_recording = body['name_recording'][0]
    bucket = "grabaciones-bbb"
    key = "AKIA3F34ORW44MKOCL5G"
    secret = "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"
    print("--------------------------url meeting ---------------------")
    print (meet_url)
    comando = "node liveJoin.js "+ meet_url +" "+name_recording +".webm 0 True "+bucket+" "+key+" "+secret
    print (comando)
    print("--------------------------------------------------")
    commands = ['cd',
                'pwd',
                'HOME=/var/snap/amazon-ssm-agent/current/bbb-recorder',
                'export HOME',
                'cd',
                'pwd',
                comando]
    instance_ids = [instance_id]
    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response,default = myconverter)
    }
    print(response)

    return response

def getUrlRecording(event, context):
    body = urllib.parse.parse_qs(event['body'])
    clave = body['clave'][0]

    try:
        response = s3Client.generate_presigned_url('get_object',
                                                    Params={'Bucket': 'grabaciones-bbb',
                                                            'Key': clave},
                                                    ExpiresIn=3000)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL

    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response

def liveBroadcasting(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    url_meet = body['url_meet'][0]
    url_rtmp = body['url_rtmp'][0]
    clave_rtmp = body['clave_rtmp'][0]


    #"rtmpUrl": "rtmp://a.rtmp.youtube.com/live2/MyKey"
    #comando_rtmp = "'rtmpUrl' : 'rtmp:\/\/a.rtmp.youtube.com\/live2\/"+clave_rtmp+"'"
    #comando_rtmp ="sed -i '2s/.*/"+comando_rtmp +"/' config.json"

    commands = ['cd',
                'pwd',
                'HOME=/var/snap/amazon-ssm-agent/current/bbb-recorder',
                'export HOME',
                'cd',
                'pwd',
                "node ffmpegServer.js "+url_rtmp+"",
                ]

    instance_ids = [instance_id]
    response1 = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    commands = ['cd',
                'pwd',
                'HOME=/var/snap/amazon-ssm-agent/current/bbb-recorder',
                'export HOME',
                'cd',
                'pwd',
                'node liveRTMP.js '+url_meet
                ]

    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response1, default=myconverter)
    }
    print(response)

    return response
