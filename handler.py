import json
import boto3
import os
import time
import urllib.parse
import hashlib
import datetime


ec2Client = boto3.client('ec2',region_name='us-east-2')
sqsClient = boto3.client('sqs', region_name='us-east-2')
dynamodbClient = boto3.client("dynamodb",region_name='us-east-2')
ssmClient = boto3.client("ssm",region_name='us-east-2')
cloudwatchClient = boto3.client("cloudwatch",region_name='us-east-2')

QUEUE_URL = os.environ['PENDING_ORDER_QUEUE']
TABLA_SERVIDOR = os.environ['TABLA_SERVIDOR']

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


def hello(event, context):


#    body = {
#        "message": "hola mundo",
#        "nombre": event['nombre'],
#        "apellido" : event['apellido'],
#    }

    body = {
        "input": event
    }

    body = urllib.parse.parse_qs(event['body'])
    print(body['nombre'])
    print(body['apellido'])

    response = {
        "statusCode": 200,
        "body": json.dumps(event['body'])
    }
    return response


def request_servidor_spot():
    print("creando request servidor")

    dryRun = False

    response = ec2Client.request_spot_instances(
        DryRun=dryRun,
        InstanceCount=1,
        LaunchSpecification={
            'SecurityGroupIds': ['sg-0c42b50b83ee74a02'],
            'ImageId': "ami-03ffa9b61e8d2cfda", #ubuntu
            #'ImageId': "ami-0502107cde7d4459c",
            'InstanceType': 't3a.medium',
            'KeyName': "escool-key",
            'IamInstanceProfile': {
                'Arn': 'arn:aws:iam::768523275705:instance-profile/AmazonSSMRoleForInstancesQuickSetup',
            },
        },
        SpotPrice='0.012',
        Type='persistent',
        InstanceInterruptionBehavior='stop'
    )
    print(response)
    request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    return request_id

def get_spot_resquest(request_id):
    print("get instance_id spot request: "+request_id)

    dryRun = False

    response = ec2Client.describe_spot_instance_requests(
        DryRun=dryRun,
        SpotInstanceRequestIds=[
            request_id,
        ],
    )
    print(response)
    instance_id = response['SpotInstanceRequests'][0]['InstanceId']
    return instance_id

def asociar_ip_elasctica(instance_id):
    dryRun = False
    # Allocate an elastic IP
    eip = ec2Client.allocate_address(DryRun=dryRun, Domain='vpc')
    # Associate the elastic IP address with the instance launched above
    if eip:
        ec2Client.associate_address(
            DryRun=dryRun,
            InstanceId=instance_id,
            AllocationId=eip["AllocationId"])

    return eip["PublicIp"]

def crear_record(record,ip):
    print("creando record "+ record)
    route53 = boto3.client('route53', region_name='us-east-2')

    response = route53.change_resource_record_sets(
        HostedZoneId='Z093093132ENBV1TCQXVM',
        ChangeBatch={
            'Comment': 'comment',
            'Changes': [
                {
                    'Action': 'UPSERT', #crea o actualiza
                    'ResourceRecordSet': {
                        'Name': record,
                        'Type': 'A',
                        'TTL': 300,
                        'ResourceRecords': [
                            {
                                'Value': ip
                            },
                        ],
                    }
                },
            ]
        }
    )
    print (response)
    return response

def get_ip_publica(instance_id):

    response = ec2Client.describe_instances(
        InstanceIds=[ instance_id ],
        DryRun=False,
    )
    print (response)
    return  response['Reservations'][0]['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp']

def inicializarServidor(event, context):
    request_id = request_servidor_spot()
    time.sleep(3)
    instance_id = get_spot_resquest(request_id)

    print("----------Esperar instance_running-----------")
    waiter = ec2Client.get_waiter('instance_running')
    waiter.wait(
        InstanceIds=[instance_id],
        DryRun=False,
        WaiterConfig={
            'Delay': 1,
            'MaxAttempts': 20
        }
    )
    print("---------- instance_running-----------")

    #ip_public = asociar_ip_elasctica(instance_id)
    ip_public = get_ip_publica(instance_id)
    dominio = '.escool.online'
    subdominio = instance_id[15:]
    crear_record(subdominio+dominio,ip_public)

    response = dynamodbClient.put_item(
        TableName=TABLA_SERVIDOR,
        Item={
            'instance_id': {'S': instance_id},
            'request_id': {'S': request_id},
            'ip_publica': {'S': ip_public},
            'subdominio': {'S': subdominio+dominio},
            'estado' : {'S': 'disponible'}
        }
    )
    print(response)

    body = {
        "request_id": request_id,
        "instance_id": instance_id,
        "ip_public" : ip_public,
        "url" : subdominio+dominio,
        "estado" : 'disponible'
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
    return response

def crearServidor(event, context):
    print("creando request servidor")


    dryRun = False

    response = ec2Client.request_spot_instances(
        DryRun=dryRun,
        InstanceCount=1,
        LaunchSpecification={
            'SecurityGroupIds': ['sg-0c42b50b83ee74a02'],
            'ImageId': "ami-03ffa9b61e8d2cfda",
            #'InstanceType': 't3a.medium',
            'InstanceType': 't3a.large',
            'KeyName': "escool-key",
        },
        SpotPrice='0.012',
        Type='persistent',
        InstanceInterruptionBehavior='stop'
    )
    print(response)

    request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']

    response = sqsClient.send_message(
        QueueUrl= QUEUE_URL,
        MessageBody=request_id,
        DelaySeconds=1,
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }

    print(response)
    return response

"""
def startServidor(event, context):

    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]

    response = ec2Client.start_instances(InstanceIds=[instance_id], DryRun=False)

    print(response)
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    return response
"""
def startOneServidor(event, context):
    # segundo 0 : el api solo puede recibir un response dentro de los 30 segundos
    # segundo 15 : la instancia en running demora 15 segundos
    # 3 minutos 30 segundo : la instancia en estado ok demora 60*3 -15 =  165 segundos
    # 4 minutos : reiniciando BBB y estado disponible
    #total de la funcion = 4*60 = 240 segundos

    print("---------------Buscando servidores apagados-----------")
    servidor = dynamodbClient.scan(
        TableName=TABLA_SERVIDOR,
        FilterExpression="estado=:a",
        ExpressionAttributeValues={
            ':a': {
                'S': 'apagado',
            }
        },
    )
    """
    for servidor in servidores['Items']:
        print("---------------servidor apagado :-----------")
        print(servidor['instance_id']['S'])
        instance_id = servidor['instance_id']['S']
        record = servidor['subdominio']['S']
        starServidor(instance_id,record)
    """
    instance_id = servidor['Items'][0]['instance_id']['S']
    record = servidor['Items'][0]['subdominio']['S']
    response = ec2Client.start_instances(InstanceIds=[instance_id], DryRun=False)

    print(response)

    print("----------Esperar instance_running-----------")
    waiter = ec2Client.get_waiter('instance_running')
    waiter.wait(
        InstanceIds=[instance_id],
        DryRun=False,
        WaiterConfig={
            'Delay': 1,
            'MaxAttempts': 20
        }
    )
    print("---------- instance_running-----------")
    #15 segundos
    ip_public = get_ip_publica(instance_id)
    time.sleep(2)
    response_record = crear_record(record,ip_public)
    print("-----------actualizando record y la ip ----------------")
    print (response_record)
    print("---------esperar status ok ------------------- ")
    waiter = ec2Client.get_waiter('instance_status_ok')
    waiter.wait(
        InstanceIds=[instance_id],
        DryRun=False,
        WaiterConfig={
            'Delay': 10,
            'MaxAttempts': 20
        }
    )
    #165 segundos
    print("-----status ok --------")
    setEstadoDynamo(instance_id, 'ok')


    print("----------actualizando ip en BBB---------------")
    response_update = updateIp(instance_id, ip_public)
    # 30 segundos
    print(response_update)
    print("-----reiniciando BBB --------")
    time.sleep(15)
    setEstadoDynamo(instance_id, 'disponible')

    return True

def setEstadoDynamo(instance_id , estado):
    response = dynamodbClient.update_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id': {
                'S': instance_id,
            }
        },
        UpdateExpression="set estado=:a",
        ExpressionAttributeValues={
            ':a': {
                'S': estado,
            }
        },
        ReturnValues="UPDATED_NEW"
    )

    print(response)
    return response

def stopAllServidores(event, context):
    print("---------------deteniendo servidores----------------")
    print (event)
    instance_id = 'i-0297e2055ffb990bd'
    response = ec2Client.stop_instances(InstanceIds=[instance_id], DryRun=False)

    print(response)
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    return response

def actualizarEstado(event, context):
    print (event)
    instance_id = event['detail']['instance-id']
    state = event['detail']['state']

    if state == 'stopped':
        time.sleep(30) #ec2 spot no puede ser encendido inmediatamente
        response = setEstadoDynamo(instance_id,'apagado')
    #if state == 'running':
    #    response = setEstadoDynamo(instance_id, 'running')
    # estara disponible cuando se actualice los ip y reiniciar BBB

    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)
    return response

def prepararServidor(event, context):
    print("Preparando servidor")
    print(event)
    request_id = event['Records'][0]['body']

    dryRun = False

    response = ec2Client.describe_spot_instance_requests(
        DryRun=dryRun,
        SpotInstanceRequestIds=[
            request_id,
        ],
    )
    print(response)
    instance_id = response['SpotInstanceRequests'][0]['InstanceId']

    print("----------Esperar instance_running-----------")
    waiter = ec2Client.get_waiter('instance_running')
    waiter.wait(
        InstanceIds=[
            instance_id,
        ],
        DryRun=False,
        WaiterConfig={
            'Delay': 1,
            'MaxAttempts': 20
        }
    )
    print("---------- instance_running-----------")

    '''
    try:
        eip = ec2Client.allocate_address(DryRun=dryRun, Domain='vpc')
    except:
        print("no hay mas ip")
    # Associate the elastic IP address with the instance launched above
    if eip:
        ec2Client.associate_address(
            DryRun=dryRun,
            InstanceId= instance_id,
            AllocationId=eip["AllocationId"])

        ip_elastica = eip["PublicIp"]

        # crear record
        subdominio = 'room0' #+instance_id[15:len(instance_id)-1]
        crear_record(subdominio,ip_elastica)
    '''

    """
    server: {
        instance_id: String,
        request_id: String,
        status: READY_FOR_DELIVERY / DELIVERED
    }
    """
    response = dynamodbClient.put_item(
        TableName= TABLA_SERVIDOR,
        Item={
            'instance_id': { 'S' : instance_id },
            'request_id' : { 'S' : request_id  },
            'ip_publica' : { 'S' : ip_elastica},
            'subdominio' : { 'S' : subdominio},
            'estado' : {}
        }
    )
    print(response)
    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    return response

def instalarBBB(event, context):

    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    dominio = '.escool.online'
    subdominio = instance_id[15:]

    commands = ['apt-get update',
        'wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -v xenial-22 -s '+subdominio+dominio+' -e josebarrientosq@gmail.com -g' ]
    instance_ids = [instance_id]
    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    print(response)
    response = {
        "statusCode": 200,
        "body": json.dumps(response,default = myconverter)
    }
    return response

def getApisecret(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]

    commands = ['bbb-conf --secret']
    instance_ids = [instance_id]
    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )
    print(response)

    command_id = response['Command']['CommandId']

    time.sleep(2)
    response = ssmClient.get_command_invocation(
        CommandId=command_id,
        InstanceId= instance_id,
    )

    apisecret = response['StandardOutputContent'].split()[3]
    print(apisecret)


    #---------------
    response = dynamodbClient.update_item(
        TableName=TABLA_SERVIDOR,
        Key={
            'instance_id': {
                'S' : instance_id,
            }
        },
        UpdateExpression="set apisecret=:a",
        ExpressionAttributeValues={
            ':a': {
                'S' : apisecret,
            }

        },
        ReturnValues="UPDATED_NEW"
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response)
    }
    print(response)

    return response



def instalarBBBrecorder(event, context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    """
    commands = ['apt install xvfb',
                'curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add',
                'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list',
                'apt-get -y install google-chrome-stable',
                'git clone https://gitlab.com/josebarrientosq/bbb_recorder.git',
                'npm install aws-sdk',
                'cd',
                'pwd',
                'HOME=/var/snap/amazon-ssm-agent/current/bbb-recorder',
                'export HOME',
                'cd',
                'pwd',
                'npm install --ignore-scripts'
                'apt-get -y update',
                ]
    """
    commands = ['']
    instance_ids = [instance_id]
    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response,default = myconverter)
    }
    print(response)

    return response

def alarma_cpu_apagar(instance_id):
    cloudwatchClient.put_metric_alarm(
        AlarmName='Web_Server_CPU_Utilization '+instance_id,
        ComparisonOperator='LessThanOrEqualToThreshold',
        EvaluationPeriods=1,
        MetricName='CPUUtilization',
        Namespace='AWS/EC2',
        Period=300,
        Statistic='Average',
        Threshold=4.0,
        AlarmDescription='alarma cuando el uso de cpu es menor a 4%',
        Dimensions=[
            {
              'Name': instance_id,
              'Value': 'INSTANCE_ID'
            },
        ],
        Unit='Seconds',
        AlarmActions=[
            'arn:aws:automate:us-east-2:ec2:stop',
        ],
    )

def funciontest(event,context):
    body = urllib.parse.parse_qs(event['body'])
    instance_id = body['instance_id'][0]
    ip_public = body['ip_public'][0]
    response= updateIp(instance_id, ip_public)
    response = {
        "statusCode": 200,
        "body": json.dumps(response, default=myconverter)
    }
    print(response)

    return response

def updateIp(instance_id , ip_public):
    print("-----------update ip BBB-------")
    print(instance_id)
    print(ip_public)
    comando1 = '''sed -i '302s/.*/<X-PRE-PROCESS cmd="set" data="external_rtp_ip='''+ip_public+'''"\/>/' vars.xml'''
    comando2 = '''sed -i '315s/.*/<X-PRE-PROCESS cmd="set" data="external_sip_ip='''+ip_public+'''"\/>/' vars.xml'''
    comando3 = '''sed -i '2s/.*/proxy_pass https:\/\/'''+ip_public+''':7443;/' sip.nginx '''
    comando4 = '''sed -i '64s/.*/ip: '''+ip_public+'''/' default.yml'''
    comando5 = '''sed -i '109s/.*/<param name="wss-binding"  value="'''+ip_public+''':7443"\/>/' external.xml'''
    comando6 = '''ip addr add '''+ip_public+'''/32 dev lo'''
    commands = ['cd',
                'pwd',
                'HOME=/opt/freeswitch/conf/',
                'export HOME',
                'cd',
                'pwd',
                comando1,
                comando2,
                'cd',
                'pwd',
                'HOME=/etc/bigbluebutton/nginx/',
                'export HOME',
                'cd',
                'pwd',
                comando3,
                'cd',
                'pwd',
                'HOME=/usr/local/bigbluebutton/bbb-webrtc-sfu/config/',
                'export HOME',
                'cd',
                'pwd',
                comando4,
                'cd',
                'pwd',
                'HOME=/opt/freeswitch/conf/sip_profiles/',
                'export HOME',
                'cd',
                'pwd',
                comando5,
                comando6,

                'bbb-conf --restart'
                ]

    instance_ids = [instance_id]
    response = ssmClient.send_command(
        DocumentName="AWS-RunShellScript",  # One of AWS' preconfigured documents
        Parameters={'commands': commands},
        InstanceIds=instance_ids,
    )

    response = {
        "statusCode": 200,
        "body": json.dumps(response,default = myconverter)
    }
    print(response)
    print("------ ip actualizadas y reiniciando BBB------------")


    return response

